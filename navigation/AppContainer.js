import { createStackNavigator } from 'react-navigation-stack';
import welcome from '../screens/welcome'
import login from '../screens/login'




export default createStackNavigator({
  Welcome: {screen:welcome},
  Login:{screen:login},
}, {
  initialRouteName: 'Welcome',
  headerMode:'none'
});